
local mykeybinding = {}

---
local awesome    = awesome
local mouse      = mouse
local client     = client
local awful      = require("awful")
local menubar    = require("menubar")
local cyclefocus = require('cyclefocus')
local scratch    = require("scratch")

local utils      = require("myutils")
local profile    = require("profile")
local vars       = profile.req("vars")

-- see myvars.lua
local terminal      = vars.terminal
local editor        = vars.editor
-- local editorCmd     = vars.editorCmd

local keyAlt   = vars.keyAlt
local modkey   = vars.keySuper

------------------- setup cyclefocus

-- Should clients be raised during cycling?
cyclefocus.raise_clients = false
--cyclefocus.raise_clients = true

-- Should clients be focused during cycling?
cyclefocus.focus_clients = true

-- Values: "top_right", "top_left", "bottom_left", "bottom_right". 
--cyclefocus.naughty_preset.position = 'top_left'
cyclefocus.naughty_preset.position = 'bottom_right'

-- How many entries should get displayed before and after the current one?
cyclefocus.display_next_count = 5
cyclefocus.display_prev_count = 0  -- only 0 for prev, works better with naughty notifications.

--cyclefocus.cycle_filters = {
    ----function(c, source_c) return not c.minimized end,
    --function(c, source_c) return true end,
--}

local function windowPrev()
    
    -- hack
    local tag = awful.tag.selected()
    for i=1, #tag:clients() do
        tag:clients()[i].minimized=false 
    end

    awful.client.focus.byidx(-1)
    if client.focus then 
        client.focus:raise() 
    end
end

local function windowNext()
    --awful.util.spawn("xwinmosaic")
    
    -- hack
    local tag = awful.tag.selected()
    for i=1, #tag:clients() do
        tag:clients()[i].minimized=false 
    end
    
    awful.client.focus.byidx(1)
    if client.focus then 
        client.focus:raise() 
    end
    
end


function mykeybinding.globalkeys(d)
    local globalkeys = awful.util.table.join(
        awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
        awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
        awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

        awful.key({ keyAlt, "Shift"   }, "Tab", windowPrev),
        awful.key({ keyAlt,           }, "Tab", windowNext),
        awful.key({ modkey,           }, "j", windowPrev),
        awful.key({ modkey,           }, "k", windowNext),
        --awful.key({ modkey,           }, "w", function () d.mymainmenu:show({keygrabber=true}) end),
        awful.key({ modkey,           }, "w", function () d.mymainmenu:show() end),

        -- Layout manipulation
        awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end),
        awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end),
        awful.key({ modkey, "Control" }, "j", function () awful.screen.focus_relative( 1) end),
        awful.key({ modkey, "Control" }, "k", function () awful.screen.focus_relative(-1) end),
        awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
        awful.key({ modkey,           }, "Tab",
            function ()
                awful.client.focus.history.previous()
                if client.focus then
                    client.focus:raise()
                end
            end),

        -- Standard program
        awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
        awful.key({ modkey,           }, "t", function () awful.util.spawn(vars.terminalAlt) end),
        awful.key({ modkey, "Control" }, "r", awesome.restart),
        awful.key({ modkey, "Shift"   }, "q", awesome.quit),

        awful.key({ modkey,           }, "Next",  function () awful.tag.incmwfact( 0.05)    end),
        awful.key({ modkey,           }, "Prior", function () awful.tag.incmwfact(-0.05)    end),
        awful.key({ modkey, "Shift"   }, "Prior", function () awful.tag.incnmaster( 1)      end),
        awful.key({ modkey, "Shift"   }, "Next",  function () awful.tag.incnmaster(-1)      end),
        awful.key({ modkey, "Control" }, "Prior", function () awful.tag.incncol( 1)         end),
        awful.key({ modkey, "Control" }, "Next",  function () awful.tag.incncol(-1)         end),
        awful.key({ modkey,           }, "space", function () awful.layout.inc(d.layouts,  1) end),
        awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(d.layouts, -1) end),

        awful.key({ modkey, "Control" }, "n", awful.client.restore),
        
        awful.key({ modkey, "Shift"   }, "Next",  function () awful.client.moveresize( 5,  5, -10, -10) end),
        awful.key({ modkey, "Shift"   }, "Prior", function () awful.client.moveresize(-5, -5,  10,  10) end),
		awful.key({ modkey, "Shift"   }, "Down",  function () awful.client.moveresize(  0,  5,   0,   0) end),
        awful.key({ modkey, "Shift"   }, "Up",    function () awful.client.moveresize(  0, -5,   0,   0) end),
        awful.key({ modkey, "Shift"   }, "Left",  function () awful.client.moveresize(-5,   0,   0,   0) end),
        awful.key({ modkey, "Shift"   }, "Right", function () awful.client.moveresize( 5,   0,   0,   0) end),
		
		awful.key({ modkey, "Control" }, "Next",  function () awful.client.moveresize( 20,  20, -40, -40) end),
        awful.key({ modkey, "Control" }, "Prior", function () awful.client.moveresize(-20, -20,  40,  40) end),
        awful.key({ modkey, "Control" }, "Down",  function () awful.client.moveresize(  0,  20,   0,   0) end),
        awful.key({ modkey, "Control" }, "Up",    function () awful.client.moveresize(  0, -20,   0,   0) end),
        awful.key({ modkey, "Control" }, "Left",  function () awful.client.moveresize(-20,   0,   0,   0) end),
        awful.key({ modkey, "Control" }, "Right", function () awful.client.moveresize( 20,   0,   0,   0) end),
        
        -- alternative application switcher
        awful.key({ keyAlt, "Control"      }, "Tab", 
            function ()
                -- If you want to always position the menu on the same place set coordinates
                awful.menu.menu_keys.down = { "Down", "Tab" }
                local cmenu = awful.menu.clients({width=245}, 
                                { keygrabber=true, coords={x=525, y=330} })
            end),

        -- Prompt
        awful.key({ modkey },            "r",     function () d.mypromptbox[mouse.screen]:run() end),
        awful.key({ keyAlt },            "F2",    function () awful.util.spawn("gmrun") end),

        awful.key({ modkey }, "x",
            function ()
                awful.prompt.run({ prompt = "Run Lua code: " },
                d.mypromptbox[mouse.screen].widget,
                awful.util.eval, nil,
                awful.util.getdir("cache") .. "/history_eval")
            end),
            
        -- Menubar
        awful.key({ modkey }, "p", function() menubar.show() end),
            
        -- Sound volume
        awful.key({ modkey }, "Up", function () awful.util.spawn("amixer set Master 5%+") end),
        awful.key({ modkey }, "Down", function () awful.util.spawn("amixer set Master 5%-") end),
        awful.key({ modkey }, "m", function () awful.util.spawn("amixer set Master toggle") end),
        
        -- mpd control
        awful.key({ keyAlt, "Control" }, "c", function () awful.util.spawn("mpc toggle") end),
        awful.key({ keyAlt, "Control" }, "x", function () awful.util.spawn("mpc play") end),
        awful.key({ keyAlt, "Control" }, "v", function () awful.util.spawn("mpc stop") end),
        awful.key({ keyAlt, "Control" }, "z", function () awful.util.spawn("mpc prev") end),
        awful.key({ keyAlt, "Control" }, "b", function () awful.util.spawn("mpc next") end),  
        
        -- kalarm
        awful.key({ modkey, "Control" }, "a", 
            function ()
                awful.util.spawn(
                    "qdbus org.kde.kalarm /kalarm/MainWin org.kde.kalarm.MainWindow.show") 
            end),        
                    
        -- Screen lock
        awful.key({ modkey }, "l", function () awful.util.spawn(vars.screenlockCmd) end),
                  
        -- Scratch drop
            -- prog   - Program to run; "urxvt", "gmrun", "thunderbird"
            -- vert   - Vertical; "bottom", "center" or "top" (default)
            -- horiz  - Horizontal; "left", "right" or "center" (default)
            -- width  - Width in absolute pixels, or width percentage
            --        when <= 1 (1 (100% of the screen) by default)
            -- height - Height in absolute pixels, or height percentage
            --        when <= 1 (0.25 (25% of the screen) by default)
            -- sticky - Visible on all tags, false by default
            -- screen - Optional screen, mouse.screen by default
        --awful.key({ modkey }, "F12", 
        --    function ()            
        --        --scratch.drop(terminal, "bottom", "center", 1, 0.4, true)
        --        scratch.drop(terminal, "bottom", "center", 1, 0.4) 
        --    end),
            
        -- start second X session
        awful.key({ modkey, "Shift" }, "x", function () awful.util.spawn("xinit -- :1 vt08") end),
        
        -- Escape from keyboard focus trap (eg Flash plugin in Firefox)
        awful.key({ modkey, "Control" }, "Escape", function ()
             awful.util.spawn("xdotool getactivewindow mousemove --window %1 0 0 click --clearmodifiers 2")
        end)
    )
    
    -- Bind all key numbers to tags.
    -- Be careful: we use keycodes to make it works on any keyboard layout.
    -- This should map on the top row of your keyboard, usually 1 to 9.
    for i = 1, 9 do
        globalkeys = awful.util.table.join(globalkeys,
            --awful.key({ modkey }, "#" .. i + 9,
            awful.key({ modkey }, "F" .. i,
                      function ()
                            local screen = mouse.screen
                            local tag = awful.tag.gettags(screen)[i]
                            if tag then
                               awful.tag.viewonly(tag)
                            end
                      end),
            --awful.key({ modkey, "Control" }, "#" .. i + 9,
            awful.key({ modkey, "Control" }, "F" .. i,
                      function ()
                          local screen = mouse.screen
                          local tag = awful.tag.gettags(screen)[i]
                          if tag then
                             awful.tag.viewtoggle(tag)
                          end
                      end),
            --awful.key({ modkey, "Shift" }, "#" .. i + 9,
            awful.key({ modkey, "Shift" }, "F" .. i,
                      function ()
                          if client.focus then
                              local tag = awful.tag.gettags(client.focus.screen)[i]
                              if tag then
                                  awful.client.movetotag(tag)
                              end
                         end
                      end),
            --awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
            awful.key({ modkey, "Control", "Shift" }, "F" .. i,
                      function ()
                          if client.focus then
                              local tag = awful.tag.gettags(client.focus.screen)[i]
                              if tag then
                                  awful.client.toggletag(tag)
                              end
                          end
                      end))
    end
    
    return globalkeys
end

function mykeybinding.clientkeys()
    return awful.util.table.join(
        
        -- application switcher
        --cyclefocus.key({ keyAlt, }, "Tab", 1, {
            ---- cycle_filters as a function callback:
            ---- cycle_filters = { function (c, source_c) return c.screen == source_c.screen end },

            ---- cycle_filters from the default filters:
            --cycle_filters = { cyclefocus.filters.same_screen, cyclefocus.filters.common_tag },
            ----keys = {'Tab', 'ISO_Left_Tab'}  -- default, could be left out
        --}),
        -- don't work :(
        --cyclefocus.key({ keyAlt, "Shift" }, "Tab", -1, {
            --cycle_filters = { cyclefocus.filters.same_screen, cyclefocus.filters.common_tag },
        --}),
        
        awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
        awful.key({ modkey, "Shift"   }, "c",      function (c) c:kill()                         end),
        awful.key({ keyAlt,           }, "F4",     function (c) c:kill()                         end),
        awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
        awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
        awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
        --awful.key({ modkey, "Shift"   }, "r",      function (c) c:redraw()                       end),
        awful.key({ modkey,           }, "t",      function (c) c.ontop = not c.ontop            end),
        awful.key({ modkey,           }, "n",
            function (c)
                -- The client currently has the input focus, so it cannot be
                -- minimized, since minimized clients can't have the focus.
                c.minimized = true
            end),
        awful.key({ modkey, "Control" }, "m",
            function (c)
                c.maximized_horizontal = not c.maximized_horizontal
                c.maximized_vertical   = not c.maximized_vertical
            end)
    )
end

return mykeybinding
