
local mywidgets = {}

-- Standard awesome library
local awful      = require("awful")
-- Theme handling library
local beautiful  = require("beautiful")
-- Widget and layout library
local wibox      = require("wibox")
-- Vicious widget library
local vicious    = require("vicious")
-- Blingbling widget library
local blingbling = require("blingbling")

local dbus       = require("dbus")
------------------

local utils   = require("myutils")
local profile = require("profile")
local vars    = profile.req("vars")

------------ vicious
-- enable caching of values returned by a widget type
--vicious.cache( vicious.widgets.net )
--vicious.cache( vicious.widgets.fs )
--vicious.cache( vicious.widgets.dio )
vicious.cache( vicious.widgets.cpu )
vicious.cache( vicious.widgets.mem )
--vicious.cache( vicious.widgets.dio )

-- Widget separator
function mywidgets.separator()
    separator = wibox.widget.textbox()
    -- separator:set_text(" :: ")
    separator:set_text(" | ")
    return separator
end

function mywidgets.labels()
    l = {}
    
    l.volume = wibox.widget.textbox()
    --l.volume = blingbling.text_box() 
    l.volume:set_text("♫ ")
    
    l.calendar = awful.widget.textclock(" %d.%m.%Y ")    
    --l.calendar:set_align("left")
    
    l.battery = wibox.widget.textbox()
    l.battery:set_text("⚡ ")
    
    return l
end

-- Clock widget
function mywidgets.clock()
    w = awful.widget.textclock(" %H:%M ")
    w:set_align("right")
    return w
end

-- Keyboard widget
function mywidgets.kbd()
    awful.util.spawn('kbdd')

    kbd = wibox.widget.textbox()
    kbd:set_text(" ✡ US")
    --kbd:set_name("kbdwidget")
    dbus.request_name("session", "ru.gentoo.kbdd")
    dbus.add_match("session", "interface='ru.gentoo.kbdd',member='layoutChanged'")
    dbus.connect_signal("ru.gentoo.kbdd", function(...)
        local data = {...}
        local layout = data[2]
        local lts = {[0] = "✡ US", [1] = "☭ RU"}
        --kbdwidget:set_markup(" " .. lts[layout])
        kbd:set_text(" " .. lts[layout] .. " ")
        end
    )
    return kbd
end

-- CPU widget
function mywidgets.cpu()
    w = awful.widget.progressbar()    
    -- progressbar properties
    w:set_width(vars.widget.cpu.width)
    w:set_vertical(true)
    w:set_background_color(beautiful.bg_focus)
    w:set_color(beautiful.widget.cpu_fg)
    --w:set_color({ type = "linear", from = { 0, 0 }, to = { 0, 20 }, stops = { { 0, "#ff0000" }, { 0.5, "#00ff00" }, { 1, "#0000ff" } }})
    -- Register widget
    vicious.register(w, vicious.widgets.cpu, "$1", vars.widget.cpu.update)
    return w
end

-- Memory widget
function mywidgets.memory()
    w = awful.widget.progressbar()    
    -- progressbar properties
    w:set_width(vars.widget.mem.width)
    w:set_vertical(true)
    w:set_background_color(beautiful.bg_focus)
    w:set_color(beautiful.widget.mem_fg)
    --w:set_color({ type = "linear", from = { 0, 0 }, to = { 0, 20 }, stops = { { 0, "#ff0000" }, { 0.5, "#00ff00" }, { 1, "#0000ff" } }})
    -- Register widget
    vicious.register(w, vicious.widgets.mem, "$1", vars.widget.mem.update)
    return w
end

-- Swap widget
function mywidgets.swap()
    w = awful.widget.progressbar()    
    -- progressbar properties
    w:set_width(7)
    w:set_vertical(true)    
    w:set_background_color(beautiful.bg_focus)
    w:set_color(beautiful.widget.swap_fg)
    -- Register widget
    vicious.register(w, vicious.widgets.mem, "$5", 2)
    return w
end

---- Battery widget
--function mywidgets.battery()
    ---- batcharge_label = wibox.widget.textbox()
    ---- batcharge_label:set_markup("⚡ ")

----     battery_charge=blingbling.progress_bar.new() 
----     battery_charge:set_height(19)
----     battery_charge:set_width(13)
----     battery_charge:set_v_margin(3)
----     battery_charge:add_value(0.5)
----     battery_charge:set_show_text(true)
----     battery_charge:set_horizontal(false)
----     battery_charge:set_graph_color(beautiful.motive)
----     battery_charge:set_text_color(beautiful.bg_focus_color)
----     battery_charge:set_background_text_color("#00000000")
----     battery_charge:set_label("")
----     battery_charge.info_path = "/sys/class/power_supply/BAT0/"
--end

function mywidgets.volume()
    --w = blingbling.volume({height = 18, width = 40, bar =true, show_text = true, label ="$percent%"})
    --w:update_master()
    --w:set_master_control()

    w = blingbling.volume()
    w:set_height(vars.widget.volume.height)
    w:set_v_margin(vars.widget.volume.vmargin)
    w:set_width(vars.widget.volume.width)
    w:update_master()
    w:set_master_control()
    w:set_bar(true)
    --w:set_background_graph_color("#444444")--beautiful.bg_focus)
    --w:set_graph_color(beautiful.motive)--beautiful.fg_normal)

    return w
end

function mywidgets.calendar(linkedWidget)
    
    w = blingbling.calendar({ widget = linkedWidget})
    w:set_link_to_external_calendar(true)
    
    w:set_days_of_month_widget_style(beautiful.widget.calendar.daysOfMonthStyle)    
    w:set_prev_next_widget_style(beautiful.widget.calendar.prevNextStyle)
    w:set_focus_widget_style(beautiful.widget.calendar.focusStyle)
    w:set_weeks_number_widget_style(beautiful.widget.calendar.weeksNumberStyle)
    w:set_days_of_week_widget_style(beautiful.widget.calendar.daysOfWeekStyle)
    w:set_corner_widget_style(beautiful.widget.calendar.cornerStyle)
    w:set_current_day_widget_style(beautiful.widget.calendar.currentDayStyle)

    return w
end

--function mywidgets.udisksGlue()
    --w = blingbling.udisks_glue.new(beautiful.icon.udisks_glue)
    --w:set_mount_icon(beautiful.icon.accept)
    --w:set_umount_icon(beautiful.icon.cancel)
    --w:set_detach_icon(beautiful.icon.cancel)
    --w:set_Usb_icon(beautiful.icon.usb)
    --w:set_Cdrom_icon(beautiful.icon.cdrom)
    --awful.widget.layout.margins[w.widget]= { top = 4}
    --w.widget.resize= false

    --return w
--end

return mywidgets
