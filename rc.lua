-- Standard awesome library
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
menubar.cache_entries = true

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify({ preset = naughty.config.presets.critical,
                     title = "Oops, there were errors during startup!",
                     text = awesome.startup_errors })
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify({ preset = naughty.config.presets.critical,
                         title = "Oops, an error happened!",
                         text = err })
        in_error = false
    end)
end
-- }}}

-- {{{ Variable definitions
-- Themes define colours, icons
--beautiful.init("/usr/share/awesome/themes/default/theme.lua")tab
beautiful.init(awful.util.getdir("config") .. "/themes/eat-it/theme.lua")

-------- my helpers
local profile      = require("profile")
local vars         = profile.req("vars")
local autostart    = profile.req("autostart") -- must be after call of beautiful.init
local rules        = profile.req("rules")
local utils        = require("myutils")
local mywidgets    = require("mywidgets")
local mykeybinding = require("mykeybinding")

-- Define wallpapers
utils.setupWallpapers()

-- This is used later as the default terminal and editor to run.
-- see myvars.lua
terminal   = vars.terminal
keyAlt     = vars.keyAlt
modkey     = vars.keySuper
editor     = vars.editor
editor_cmd = vars.editorCmd

-- Table of layouts to cover with awful.layout.inc, order matters.
local layouts =
{
    awful.layout.suit.floating,
    awful.layout.suit.tile,
    --awful.layout.suit.tile.left,
    --awful.layout.suit.tile.bottom,
    --awful.layout.suit.tile.top,
    --awful.layout.suit.fair,
    --awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    awful.layout.suit.max,
    --awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier
}
-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}
for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[s] = awful.tag(vars.tagMarks, s, layouts[1])
end
-- }}}

-- Menubar configuration
menubar.utils.terminal = vars.terminalMenu -- Set the terminal for applications that require it
-- }}}

-- {{{ Menu
local fdUtils = require('freedesktop.utils')
fdUtils.terminal   = vars.terminalMenu
local fdMenu = require('freedesktop.menu')
local menuItems = fdMenu()
---- Create a laucher widget and a main menu
myawesomemenu = {
    { "manual", terminal .. " -e man awesome", fdUtils.lookup_icon( { 'help', 'help-browser'} ) },
--    { "edit config", editor_cmd .. " " .. awesome.conffile, fdUtils.lookup_icon('package_settings') },
    { "restart", awesome.restart, fdUtils.lookup_icon('gtk-refresh') },
--    { "quit", awesome.quit, fdUtils.lookup_icon('gtk-quit') }
--    { "quit", awesome.quit, fdUtils.lookup_icon('exit') }
--    { "logout", awesome.quit, fdUtils.lookup_icon('system-log-out') }
}

systemmenu = {
    { "logout", awesome.quit, fdUtils.lookup_icon('system-log-out') },
    { "reboot", 
        function()  
            -- add sending SIGTERM to all
            awful.util.spawn(
                "dbus-send --system --print-reply \
                --dest=org.freedesktop.ConsoleKit \
                --type=method_call \
                /org/freedesktop/ConsoleKit/Manager \
                org.freedesktop.ConsoleKit.Manager.Restart") 
        end, 
        fdUtils.lookup_icon( { 'reload', 'xfsm-reboot'} ) 
    },
    { "poweroff", 
        function()  
            awful.util.spawn(
                "dbus-send --system --print-reply \
                --dest=org.freedesktop.ConsoleKit \
                /org/freedesktop/ConsoleKit/Manager \
                org.freedesktop.ConsoleKit.Manager.Stop") 
        end, 
        fdUtils.lookup_icon( { 'system-shutdown'} ) 
    },
}

table.insert(menuItems, { "Awesome", myawesomemenu, beautiful.awesome_icon })
table.insert(menuItems, { "Leave", systemmenu, fdUtils.lookup_icon( { 'computer'} ) })
table.insert(menuItems, { "open terminal", terminal, fdUtils.lookup_icon('terminal') })

mymainmenu = awful.menu.new({ items = menuItems })

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Create widgets

widget = {}
widget.separator     = mywidgets.separator()
widget.labels        = mywidgets.labels()
widget.clock         = mywidgets.clock()
widget.kbd           = mywidgets.kbd()
widget.cpu           = mywidgets.cpu()
widget.mem           = mywidgets.memory()
widget.swap          = mywidgets.swap()
widget.volume        = mywidgets.volume()
widget.calendar      = mywidgets.calendar(widget.labels.calendar)

-- {{{ Wibox
-- Create a textclock widget
mytextclock = awful.widget.textclock()

-- Create a wibox for each screen and add it
myTopWibox    = {}
myBottomWibox = {}

mypromptbox = {}
mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
    awful.button({        }, 1, awful.tag.viewonly),
    awful.button({ modkey }, 1, awful.client.movetotag),
    awful.button({        }, 3, awful.tag.viewtoggle),
    awful.button({ modkey }, 3, awful.client.toggletag),
    awful.button({        }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
    awful.button({        }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
)

mytasklist = {}
mytasklist.buttons = awful.util.table.join(
         awful.button({ }, 1, function (c)
                                  if c == client.focus then
                                      c.minimized = true
                                  else
                                      -- Without this, the following
                                      -- :isvisible() makes no sense
                                      c.minimized = false
                                      if not c:isvisible() then
                                          awful.tag.viewonly(c:tags()[1])
                                      end
                                      -- This will also un-minimize
                                      -- the client, if needed
                                      client.focus = c
                                      c:raise()
                                  end
                              end),
         awful.button({ }, 3, function ()
                                  if instance then
                                      instance:hide()
                                      instance = nil
                                  else
                                      instance = awful.menu.clients({ width=250 })
                                  end
                              end),
         awful.button({ }, 4, function ()
                                  awful.client.focus.byidx(1)
                                  if client.focus then client.focus:raise() end
                              end),
         awful.button({ }, 5, function ()
                                  awful.client.focus.byidx(-1)
                                  if client.focus then client.focus:raise() end
                              end))

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt()
    
    -- Create an imagebox widget which will contains an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    mylayoutbox[s] = awful.widget.layoutbox(s)
    mylayoutbox[s]:buttons(awful.util.table.join(
                           awful.button({ }, 1, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(layouts, -1) end),
                           awful.button({ }, 4, function () awful.layout.inc(layouts, 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(layouts, -1) end)))
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.filter.all, mytaglist.buttons)

    -- Create a tasklist widget
    mytasklist[s] = awful.widget.tasklist(s, awful.widget.tasklist.filter.currenttags, mytasklist.buttons)
    
    -- Create the wiboxs
    
    --------------------------
    -- Top wibox
    --------------------------
    myTopWibox[s] = awful.wibox({ position = "top", screen = s, height = vars.wibox.height })
    -- Add widgets to the wibox
    
    ---- Widgets that are aligned to the left
    local topLeftLayout = wibox.layout.fixed.horizontal()
    topLeftLayout:add(mytaglist[s])
    topLeftLayout:add(widget.separator)
    topLeftLayout:add(mypromptbox[s])
    
    local topRightLayout = wibox.layout.fixed.horizontal()
    topRightLayout:add(widget.separator)
    --topRightLayout:add(widget.labels.calendar) -- no need if calendar using
    topRightLayout:add(widget.calendar)
    topRightLayout:add(widget.separator)
    topRightLayout:add(mylayoutbox[s])
    
    local topLayout = wibox.layout.align.horizontal()
    topLayout:set_left(topLeftLayout)
    topLayout:set_right(topRightLayout)    
    myTopWibox[s]:set_widget(topLayout)
    
    --------------------------
    -- Bottom wibox
    --------------------------
    myBottomWibox[s] = awful.wibox({ position = "bottom", screen = s, height = vars.wibox.height })
    
    -- Add widgets to the wibox
    
    ---- Widgets that are aligned to the left
    local bottomLeftLayout = wibox.layout.fixed.horizontal()
    bottomLeftLayout:add(mylauncher)
    bottomLeftLayout:add(widget.separator)
    
    local bottomRightLayout = wibox.layout.fixed.horizontal()
    if s == 1 then bottomRightLayout:add(wibox.widget.systray()) end
    bottomRightLayout:add(widget.separator)
    bottomRightLayout:add(widget.labels.volume)
    bottomRightLayout:add(widget.volume)
    bottomRightLayout:add(widget.separator)
    bottomRightLayout:add(widget.kbd)
    bottomRightLayout:add(widget.separator)
    bottomRightLayout:add(widget.clock)    
    bottomRightLayout:add(widget.cpu)
    bottomRightLayout:add(widget.mem)
    bottomRightLayout:add(widget.swap)
    
    local bottomLayout = wibox.layout.align.horizontal()
    bottomLayout:set_left(bottomLeftLayout)
    bottomLayout:set_middle(mytasklist[s])
    bottomLayout:set_right(bottomRightLayout)
    myBottomWibox[s]:set_widget(bottomLayout)

end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}

-- {{{ Key bindings
local context = {}
context.mypromptbox = mypromptbox
context.mymainmenu  = mymainmenu
context.layouts     = layouts
globalkeys = mykeybinding.globalkeys(context)
clientkeys = mykeybinding.clientkeys()

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules

local commonRules = {

    -- All clients will match this rule.
    { rule = { },
          properties = { border_width = beautiful.border_width,
                         border_color = beautiful.border_normal,
                         focus        = awful.client.focus.filter,
                         keys         = clientkeys,
                         buttons      = clientbuttons } },
                         
    { rule = { class = "pinentry" },
        properties = { floating = true } },
    { rule = { class = "gimp" },
        properties = { floating = true } },
    { rule = { class = "MPlayer" },
        properties = { floating = true },
        callback = awful.placement.centered,  
    },
    { rule = { class = "mplayer2" },
        properties = { floating = true },
        callback = awful.placement.centered,  
    },
    { rule = { class = "Yakuake" },
        properties = { border_width = 0 } },
    
    {   rule_any = { 
            class = {"Download"}, instance = { "Download", "Dialog" },
            name = {"About "}, role = {"About"}
        }, 
        properties = { 
            floating = true, 
            maximized_vertical   = false,
            maximized_horizontal = false,
            maximized            = false,
            --border_width = 0,
        }, 
        callback = awful.placement.centered, 
    },
}

-- Join common and profile rules
awful.rules.rules = awful.util.table.join(commonRules, rules.get(clientkeys, clientbuttons, tags))
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c, startup)
    -- Enable sloppy focus
    c:connect_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        awful.client.setslave(c)

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            awful.placement.no_overlap(c)
            awful.placement.no_offscreen(c)
        end
    end

    local titlebars_enabled = false
    if titlebars_enabled and (c.type == "normal" or c.type == "dialog") then
        -- buttons for the titlebar
        local buttons = awful.util.table.join(
                awful.button({ }, 1, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.move(c)
                end),
                awful.button({ }, 3, function()
                    client.focus = c
                    c:raise()
                    awful.mouse.client.resize(c)
                end)
                )

        -- Widgets that are aligned to the left
        local left_layout = wibox.layout.fixed.horizontal()
        left_layout:add(awful.titlebar.widget.iconwidget(c))
        left_layout:buttons(buttons)

        -- Widgets that are aligned to the right
        local right_layout = wibox.layout.fixed.horizontal()
        right_layout:add(awful.titlebar.widget.floatingbutton(c))
        right_layout:add(awful.titlebar.widget.maximizedbutton(c))
        right_layout:add(awful.titlebar.widget.stickybutton(c))
        right_layout:add(awful.titlebar.widget.ontopbutton(c))
        right_layout:add(awful.titlebar.widget.closebutton(c))

        -- The title goes in the middle
        local middle_layout = wibox.layout.flex.horizontal()
        local title = awful.titlebar.widget.titlewidget(c)
        title:set_align("center")
        middle_layout:add(title)
        middle_layout:buttons(buttons)

        -- Now bring it all together
        local layout = wibox.layout.align.horizontal()
        layout:set_left(left_layout)
        layout:set_right(right_layout)
        layout:set_middle(middle_layout)

        awful.titlebar(c):set_widget(layout)
    end
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

-- Autorun programs
autostart.run()
