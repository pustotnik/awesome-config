---

local rules = {}

local awful     = require("awful")
local beautiful = require("beautiful")

function rules.get(clientkeys, clientbuttons, tags)

    return 
    {        
        -- Set Firefox to always map on tags number 3 of screen 1.
        { rule = { class = "Firefox" },
          properties = { tag = tags[1][3], switchtotag = true } },
        { rule = { class = "Thunderbird" },
          properties = { tag = tags[1][3], floating = false, switchtotag = false } },
          
    }

end

return rules
