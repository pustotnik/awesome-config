---

local autostart = {}

local theme   = require("beautiful")
local utils   = require("../../myutils")
--local profile = require("profile")
--local vars    = profile.req("vars")

local autorunApps =  
{
    "wmname LG3D", -- fix java apps problem
}

local runOnceApps = 
{
    "xcompmgr -cfF &",
    utils.getTinymountCmdline(),
    "guake",
    "xscreensaver -no-splash &",
    -- "emacs --daemon",
    -- "thunderbird &",
}

function autostart.run()
    utils.doAutostart(autorunApps, runOnceApps)
end

return autostart
