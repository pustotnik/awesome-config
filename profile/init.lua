---

local profile = {}

local awful         = require("awful")
local file_readable = awful.util.file_readable

local utils = require("../myutils")


profile.defaultName = 'netbook'

local profileDir = awful.util.getdir("config") .. "/profile/"

local function detectProfile()
    if file_readable(profileDir .. 'current') then
        return 'current'
    else
        return profile.defaultName
    end
end

profile.name = detectProfile()

function profile.req(name)    
    return require("profile." .. profile.name .. "." .. name)
end

return profile
