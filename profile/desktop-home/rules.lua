---

local rules = {}

local awful     = require("awful")
local beautiful = require("beautiful")

function rules.get(clientkeys, clientbuttons, tags)

    return 
    {
        --{ rule = { class = "Kalarm" },
            --properties = { 
                --floating = true, 
            --},
            --callback = awful.titlebar.add
        --},
        
        -- tag 1
        { rule = { class = "Krusader" },
            properties = { 
                tag = tags[1][1], floating = true, switchtotag = true,
            } 
        },
        
        -- tag 2
        { rule = { class = "Goldendict" },
            properties = { 
                tag = tags[1][2], floating = true, switchtotag = false, 
                maximized_vertical = true,
            } 
        },
        { rule = { class = "Sonata" },
            properties = { 
                tag = tags[1][2], floating = true, switchtotag = false,
                maximized_vertical = true,
            } 
        },
        
        -- tag 3        
        { rule = { class = "Firefox", role = "browser" },
            properties = { 
                tag = tags[1][3], floating = true, switchtotag = true, 
                maximized = true,
            } 
        },
        { rule = { class = "Thunderbird" }, except = { role = "About" },
            properties = { 
                tag = tags[1][3], floating = true, switchtotag = false,
                maximized = true,
            } 
        },
        -- tag 4
        
        -- tag 5
        { rule = { class = "Eiskaltdcpp-qt" },
            properties = { 
                tag = tags[1][5], floating = true, switchtotag = false,
                maximized = true,
            } 
        },
        
        -- tag 6
    }

end

return rules
