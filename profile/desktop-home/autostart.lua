---

local autostart = {}

local awful   = require("awful")
local theme   = require("beautiful")
local utils   = require("../../myutils")
local profile = require("profile")
local vars    = profile.req("vars")

local autorunApps =  
{
    "wmname LG3D", -- fix java apps problem
    --"kbdd",
}

local runOnceApps = 
{
    -- "xcompmgr -cfF &",
    "compton -b --config " .. 
        awful.util.getdir("config") .. 
        "/compton.conf" .. 
        " --shadow-exclude-reg='x" .. vars.wibox.height .. "+0-0'",
	"krusader &",
    utils.getTinymountCmdline(),
    --"guake",
	"yakuake",
    "xscreensaver -no-splash &",
    -- "emacs --daemon",
	"eiskaltdcpp-qt &",
	"goldendict &",
	"sonata &",
	"parcellite &",
    "kalarm --tray",
    -- "thunderbird &",
}

function autostart.run()
    utils.doAutostart(autorunApps, runOnceApps)
end

return autostart
