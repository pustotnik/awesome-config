---

local vars = {}

local os    = os
local awful = require("awful")

--vars.tagMarks = { 1, 2, 3, 4, 5, 6, 7, 8, 9 }
vars.tagMarks = { 1, 2, 3, 4, 5, 6 }
--vars.tagMarks = { "α", "β", "δ", "λ", "θ", "Ω" }

-- This is used later as the default terminal and editor to run.
-- vars.terminal = "xterm"
-- vars.terminal = "terminal"
-- vars.terminal = "urxvt"
vars.terminal     = "aterm"
--vars.terminalAlt  = "lxterminal"
vars.terminalAlt  = "konsole"
vars.terminalMenu = "aterm"
vars.editor       = os.getenv("EDITOR") or "vim"
vars.editorCmd    = vars.terminal .. " -e " .. vars.editor

-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.

vars.keySuper = "Mod4"
vars.keyAlt   = "Mod1"
vars.modkey   = vars.keySuper

--------------------------------------------
-- features settings

vars.screenlockCmd = "xscreensaver-command -lock"
-- screenlockCmd = "slock"

--automount = {}
--automount.enabled = true
-- TODO: automount.auto = false
--automount.method = "pcmanfm"
--automount.method = "thunar"
--automount.method = "udisks-glue"

--------------------------------------------
-- wibox settings

vars.wibox = {}
vars.wibox.height   = 23
--vars.wibox.position = "bottom"

--------------------------------------------
-- widgets settings

vars.widget = {}
vars.widget.cpu = {}
vars.widget.cpu.width  = 7
vars.widget.cpu.update = 1

vars.widget.mem = {}
vars.widget.mem.width  = 7
vars.widget.mem.update = 2

vars.widget.volume = {}
vars.widget.volume.height  = vars.wibox.height - 2
vars.widget.volume.vmargin = 4
vars.widget.volume.width   = 22

return vars
