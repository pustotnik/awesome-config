-- Grab environment

local io      = io
local os      = os
local table   = table
local type    = type
local ipairs  = ipairs
local pairs   = pairs
local menubar = require("menubar")

-- freedesktop.utils
local utils = {}

utils.terminal = 'xterm'

function utils.lookup_icon(icons)
    
    if type(icons) ~= 'table' then
        return menubar.utils.lookup_icon(icons)
    end
    
    local result = nil
    for _, icon in ipairs(icons) do
        result = menubar.utils.lookup_icon(icon)
        if(result) then
            return result
        end
    end

    return result
end

--- Parse a .desktop file
-- @param file The .desktop file
-- @return A table with file entries.
function utils.parse_desktop_file(file)
    -- temporary set terminal variable
    local menubarTerminal = menubar.utils.terminal
    menubar.utils.terminal = utils.terminal
    
    local result = menubar.utils.parse(file)
    
    -- restore terminal variable
    menubar.utils.terminal = menubarTerminal
    return result
end

--- Parse a directory with .desktop files
-- @param dir The directory.
-- @param icons_size, The icons sizes, optional.
-- @return A table with all .desktop entries.
function utils.parse_desktop_files(dir)
    ---- temporary set terminal variable
    --local menubarTerminal = menubar.utils.terminal
    --menubar.utils.terminal = utils.terminal
    
    --local result = menubar.utils.parse_dir(dir)
    
    ---- restore terminal variable
    --menubar.utils.terminal = menubarTerminal
    --return result

    local programs = {}
    local files = io.popen('find '.. dir ..' -maxdepth 2 -name "*.desktop" 2>/dev/null')
    for file in files:lines() do
        local program = utils.parse_desktop_file(file)
        if program then
            table.insert(programs, program)
        end
    end
    return programs
end

return utils
