---------------------------
-- My awesome theme --
---------------------------

local awful = require("awful")

theme = {}

-- Map useful functions outside
local file_readable = awful.util.file_readable

local themename = "eat-it"

local path  = {}

------------------------
-- prepare
------------------------

path.home           = os.getenv("HOME")
path.config         = awful.util.getdir("config")
path.share          = {}
path.share.dir      = "/usr/share/awesome"
if not file_readable(path.share.dir .. "/icons/awesome16.png") then
    path.share.dir  = "/usr/share/local/awesome"
end
path.share.icons    = path.share.dir .. "/icons"
path.share.themes   = path.share.dir .. "/themes"
path.themes         = path.config .. "/themes"
path.themedir       = path.themes .. "/" .. themename

if not file_readable(path.themedir .. "/theme.lua") then
    path.themes     = path.share.themes
    path.themedir   = path.themes .. "/" .. themename
end

local wallpaper1          = path.themedir .. "/background.jpeg"
local wallpaper2          = path.themedir .. "/background.jpg"
local wallpaper3          = path.themedir .. "/background.png"
local wallpaper4          = path.share.themes .. "/default/background.png"
--local wpscript            = path.home .. "/.wallpaper"

if file_readable(wallpaper1) then
    theme.wallpaper_cmd = { "awsetbg " .. wallpaper1 }
    theme.wallpaper = wallpaper1
elseif file_readable(wallpaper2) then
    theme.wallpaper_cmd = { "awsetbg " .. wallpaper2 }
    theme.wallpaper = wallpaper2
--elseif file_readable(wpscript) then
--    theme.wallpaper_cmd = { "sh " .. wpscript }
elseif file_readable(wallpaper3) then
    theme.wallpaper_cmd = { "awsetbg " .. wallpaper3 }
    theme.wallpaper = wallpaper3
else
    theme.wallpaper_cmd = { "awsetbg " .. wallpaper4 }
    theme.wallpaper = wallpaper4
end

local function rgb(red, green, blue)
  if type(red) == "number" or type(green) == "number" or type(blue) == "number" then
    return "#"..string.format("%02x",red)..string.format("%02x",green)..string.format("%02x",blue)
  else    
    return nil
  end
end

local function rgba(red, green, blue, alpha) 
  if type(red) == "number" or type(green) == "number" or type(blue) == "number" or type(alpha) == "number" then
    return "#"..string.format("%02x",red)..string.format("%02x",green)..string.format("%02x",blue)..string.format("%02x",alpha * 255)
  else
    return nil
  end
end

--colors
local bordeaux= rgb(47,28,28)
local light_bordeaux = rgba(191,64,64,0.6)
local dark_grey = "#121212"
local grey = "#444444ff"
local light_grey = "#555555"
local white = "#ffffff"
local light_white = "#999999"
local light_black = "#232323"
local red = "#b9214f"
local bright_red = "#ff5c8d"
local yellow = "#ff9800"
local bright_yellow = "#ffff00"
local black = "#000000"
local bright_black = "#5D5D5D"
local green = "#A6E22E"
local bright_green = "#CDEE69"
local blue = "#3399ff"
local bright_blue = "#9CD9F0"
local magenta = "#8e33ff"
local bright_magenta = "#FBB1F9"
local cyan = "#06a2dc"
local bright_cyan = "#77DFD8"
local widget_background = "#303030"
--local white = "#B0B0B0"
local bright_white = "#F7F7F7"
local transparent = "#00000000"

---------------------------
-- theme config
---------------------------

theme.grey              = grey
theme.light_grey        = light_grey
theme.white             = white
theme.black             = black
theme.light_black       = light_black
theme.red               = red
theme.bright_red        = bright_red
theme.yellow            = yellow
theme.bright_yellow     = bright_yellow
theme.bright_black      = bright_black
theme.green             = green
theme.bright_green      = bright_green
theme.blue              = blue
theme.bright_blue       = bright_blue
theme.magenta           = magenta
theme.bright_magenta    = bright_magenta
theme.cyan              = cyan
theme.bright_cyan       = bright_cyan
theme.widget_background = widget_background
theme.transparent       = transparent

--theme.font          = "sans 8"
--theme.font          = "droid sans 10"
theme.font          = "dejavu sans 9"

theme.bg_normal     = "#222222"
theme.bg_focus      = "#535d6c"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.border_width  = "1"
theme.border_normal = "#000000"
theme.border_focus  = "#535d6c"
theme.border_marked = "#91231c"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- Example:
--theme.taglist_bg_focus = "#ff0000"


-- Display the taglist squares
-- Uncomment to enable
--theme.taglist_squares_sel   = path.themedir .. "/taglist/squarefw.png"
--theme.taglist_squares_unsel = path.themedir .. "/taglist/squarew.png"

theme.tasklist_floating_icon = path.themedir .. "/tasklist/floatingw.png"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = path.themedir .. "/submenu.png"
theme.menu_height = "20"
theme.menu_width  = "200"

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = path.themedir .. "/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = path.themedir .. "/titlebar/close_focus.png"

theme.titlebar_ontop_button_normal_inactive = path.themedir .. "/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = path.themedir .. "/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = path.themedir .. "/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = path.themedir .. "/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = path.themedir .. "/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = path.themedir .. "/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = path.themedir .. "/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = path.themedir .. "/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = path.themedir .. "/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = path.themedir .. "/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = path.themedir .. "/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = path.themedir .. "/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = path.themedir .. "/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = path.themedir .. "/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = path.themedir ..  "/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = path.themedir .. "/titlebar/maximized_focus_active.png"

-- You can use your own command to set your wallpaper
-- theme.wallpaper_cmd = { "awsetbg /usr/share/awesome/themes/default/background.png" }

-- You can use your own layout icons like this:
theme.layout_fairh      = path.themedir .. "/layouts/fairhw.png"
theme.layout_fairv      = path.themedir .. "/layouts/fairvw.png"
theme.layout_floating   = path.themedir .. "/layouts/floatingw.png"
theme.layout_magnifier  = path.themedir .. "/layouts/magnifierw.png"
theme.layout_max        = path.themedir .. "/layouts/maxw.png"
theme.layout_fullscreen = path.themedir .. "/layouts/fullscreenw.png"
theme.layout_tilebottom = path.themedir .. "/layouts/tilebottomw.png"
theme.layout_tileleft   = path.themedir .. "/layouts/tileleftw.png"
theme.layout_tile       = path.themedir .. "/layouts/tilew.png"
theme.layout_tiletop    = path.themedir .. "/layouts/tiletopw.png"
theme.layout_spiral     = path.themedir .. "/layouts/spiralw.png"
theme.layout_dwindle    = path.themedir .. "/layouts/dwindlew.png"

theme.icon = {}
theme.icon.awesome = path.share.icons .. "/awesome16.png"

theme.icon.shutdown    = path.themedir .. "/icons/shutdown.png"
theme.icon.reboot      = path.themedir .. "/icons/reboot.png"
theme.icon.accept      = path.themedir .. "/icons/accept.png"
theme.icon.cancel      = path.themedir .. "/icons/cancel.png"
theme.icon.calendar    = path.themedir .. "/icons/calendar.png"
theme.icon.task        = path.themedir .. "/icons/task.png"
theme.icon.tasks       = path.themedir .. "/icons/tasks.png"
theme.icon.task_done   = path.themedir .. "/icons/task_done.png"
theme.icon.project     = path.themedir .. "/icons/project.png"
theme.icon.udisks_glue = path.themedir .. "/icons/udisk-glue.png"
theme.icon.usb         = path.themedir .. "/icons/usb.png"
theme.icon.cdrom       = path.themedir .. "/icons/cdrom.png"

theme.awesome_icon = theme.icon.awesome

-- Define the icon theme for application icons. If not set then the icons 
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
--theme.icon_theme = nil
theme.icon_theme = 'Tango'

--- widgets 

theme.widget = {}
theme.widget.cpu_fg   = "#ffffff"
theme.widget.mem_fg   = "#ffffff"
theme.widget.swap_fg  = "#ffffff"

-- blingbling widgets
theme.widget.calendar = {}

local commonCalStyle = 
{  
    h_margin = 0, v_margin = 0, rounded_size = 0.3, 
    background_color = widget_background 
}

local tmpStyle = {}

theme.widget.calendar.commonStyle      = commonCalStyle
theme.widget.calendar.daysOfMonthStyle = commonCalStyle
theme.widget.calendar.prevNextStyle    = commonCalStyle
theme.widget.calendar.focusStyle       = commonCalStyle
tmpStyle = awful.util.table.clone(commonCalStyle)
tmpStyle.text_color = theme.border_focus
theme.widget.calendar.weeksNumberStyle = tmpStyle
theme.widget.calendar.daysOfWeekStyle  = tmpStyle
theme.widget.calendar.cornerStyle      = tmpStyle
tmpStyle = awful.util.table.clone(commonCalStyle)
tmpStyle.background_color = theme.cyan
tmpStyle.rounded_size = {0.5,0,0.5,0}
theme.widget.calendar.currentDayStyle  = tmpStyle


return theme
