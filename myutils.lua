
local myutils = {}

-- Useful reusable functions

-- Standard awesome library
local awful   = require("awful")
-- Notification library
local naughty = require("naughty")
-- Theme handling library
local beautiful = require("beautiful")
local gears     = require("gears")

local tonumber = tonumber
local tostring = tostring
local io       = io

function myutils.runOnce(cmdLine, times)
    if not cmdLine then
        do return nil end
    end
    times = times or 1
    
    local posEnd, posBegin = 1
    posBegin, posEnd = cmdLine:find("[^%s]*", posBegin)
    if not posBegin or not posEnd then
        do return nil end
    end
    local progName = cmdLine:sub(posBegin, posEnd)
    --local args = ""
    
    --posBegin, posEnd = cmdLine:find("[^%s]+.*", posEnd + 1)
    --if posBegin and posEnd then
        --args = cmdLine:sub(posBegin, posEnd)
    --end
    
    local countProg = tonumber(awful.util.pread(
                'pgrep "' .. progName .. '" | wc -l'))
        
    if times > countProg then
        for l = countProg, times - 1 do
            --awful.util.spawn_with_shell(progName .. ' ' .. args)
            --awful.util.spawn_with_shell(cmdLine)
            --myutils.nprint(cmdLine)
            awful.util.spawn('bash -c "' .. cmdLine .. '"', true)
        end
    end
end

function myutils.doAutostart(autorunApps, runOnceApps)
    for app = 1, #runOnceApps do
        myutils.runOnce(runOnceApps[app])
    end
    for app = 1, #autorunApps do
        --awful.util.spawn_with_shell(autorunApps[app])
        awful.util.spawn('bash -c "' .. autorunApps[app] .. '"', true)
    end
end

-- *** Debug pring function *** ---
function myutils.nprint(s)
   naughty.notify( { title = "Debug print",
                     text = tostring(s),
                     timeout = 5})
end

-- scan directory, and optionally filter outputs
function myutils.scandir(directory, filter)
    local i, t, popen = 0, {}, io.popen
    if not filter then
        filter = function(s) return true end
    end
    --print(filter)
    for filename in popen('ls -a "'..directory..'"'):lines() do
        if filter(filename) then
            i = i + 1
            t[i] = filename
        end
    end
    return t
end

function myutils.getTinymountCmdline()
    local tinymountIconTheme = ""
    if beautiful.icon_theme then
        tinymountIconTheme = " --iconTheme=" .. beautiful.icon_theme
    end
    return "tinymount" .. tinymountIconTheme .. " &"
end

-- to automatically fetch images from a given directory

---- configuration - edit to your liking
--wp_index = 1
--wp_timeout  = 10
--wp_path = "/path/to/wallpapers/"
--wp_filter = function(s) return string.match(s,"%.png$") or string.match(s,"%.jpg$") end
--wp_files = scandir(wp_path, wp_filter)
 
---- setup the timer
--wp_timer = timer { timeout = wp_timeout }
--wp_timer:connect_signal("timeout", function()
 
  ---- set wallpaper to current index for all screens
  --for s = 1, screen.count() do
    --gears.wallpaper.maximized(wp_path .. wp_files[wp_index], s, true)
  --end
 
  ---- stop the timer (we don't need multiple instances running at the same time)
  --wp_timer:stop()
 
  ---- get next random index
  --wp_index = math.random( 1, #wp_files)
 
  ----restart the timer
  --wp_timer.timeout = wp_timeout
  --wp_timer:start()
--end)
 
---- initial start when rc.lua is first run
--wp_timer:start()

function myutils.setupWallpapers()
    if beautiful.wallpaper then
        for s = 1, screen.count() do
            -- http://awesome.naquadah.org/doc/api/modules/gears.wallpaper.html
            gears.wallpaper.maximized(beautiful.wallpaper, s, true)
        end
    end
end

return myutils
